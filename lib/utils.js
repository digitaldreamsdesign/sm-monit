var ChildProcess = require('child_process');
var IS_WIN = process.platform === 'win32';
var TableParser = require( 'table-parser' );
var os = require("os");
var Q = require('q');

var exports = module.exports = {};

if (!String.prototype.includes) {
  String.prototype.includes = function() {'use strict';
    return String.prototype.indexOf.apply(this, arguments) !== -1;
  };
}

exports.ps = function(args, callback) {
    ChildProcess.exec(args, function( err, stdout, stderr) {
            if (err || stderr) {
                console.log(stdout);
                return callback( err || stderr.toString() );
            }
            else {
                //console.log(stdout.toString());
                var processList = parseGrid( stdout );
                var resultList = [];

                processList.forEach(function( p ){
                    var result = true;
//                    var command = p["COMMAND"];
//                    if(command === 'firefox') {
//                        console.log(p);
//                    }
                    p['RSS'] = parseInt(p['RSS']);
                    p['%CPU'] = parseFloat(p['%CPU']);
                    if(p['RSS'] <= 0) {
                        result = false;
                    }
                    p['count'] = 1;
                    for(var x = 0; x < resultList.length; x++) {
                        if(resultList[x]['USER'] === p["USER"] && resultList[x]['COMMAND'] === p["COMMAND"]) {
                            resultList[x]['count'] = resultList[x]['count'] +1;
                            resultList[x]['%CPU'] = parseFloat(resultList[x]['%CPU']) + parseFloat(p["%CPU"]);
                            resultList[x]['%MEM'] = parseFloat(resultList[x]['%MEM']) + parseFloat(p["%MEM"]);
                            
                            if(!isNaN(p["RSS"])) {
                                resultList[x]['RSS'] = parseFloat(resultList[x]['RSS']) + parseFloat(p["RSS"]);
                                resultList[x]['RSS'] = resultList[x]['RSS'];
                            }
                            
                            result = false;
                        } else {
                            
                        }
                    }
                    
                    if( result ){
                        resultList.push( p );
                    }
                });
                
                return callback(false, resultList);
            }
        });
};

function parseGrid( output ) {
    if ( !output ) {
        return output;
    }
    var lines = output.split(/(\r?\n)/g);
    //console.log(lines[0]);
    var tableHead = [];
    var resultList = [];

    for (var i=0; i<lines.length; i++) {
        // Process the line, noting it might be incomplete.
        //console.log(lines[i]);
        var parts = lines[i].split(/ +/g);
        var p = {};
        //console.log(parts) ;
        var add = false;
        if(parts.length>1) {
            for (var j=0; j<parts.length; j++) {
                if(i === 0) {
                    tableHead.push(parts[j]);
                } else {
                    var th = tableHead[j];
                    p[th] = parts[j];
                    add = true;
                }
            }
        }
        if(add) {
            resultList.push(p);
        }
        
    }
    return resultList;
    
    //return TableParser.parse( output );
}

exports.cat = function(args, callback) {
    ChildProcess.exec( args, function( err, stdout, stderr) {
            if (err || stderr) {
                return callback( err || stderr.toString() );
            }
            else {
                //console.log(stdout.toString());
                var lines = stdout.toString().split(/\n/g);;
                var bytes = 0;
                for(var x = 0; x < lines.length; x++) {
                    
                    if(lines[x] !== '') {
                        bytes = bytes + parseInt(lines[x]);
                    }
                }
                //var receivedBytes = lines[1];
                
                return callback(false, bytes);
            }
        });
};

exports.df = function(args, callback) {
    ChildProcess.exec( args, function( err, stdout, stderr) {
            if (err || stderr) {
                return callback( err || stderr.toString() );
            }
            else {
                //console.log(stdout.toString());
                var processList = parseGrid( stdout );
                var resultList = [];
                
                processList.forEach(function( p ){
                    var result = true;
                    
                    if( result ){
                        resultList.push( p );
                    }
                });
                
                return callback(false, resultList);
            }
        });
};

exports.cpuTemp = function(args, callback) {
    ChildProcess.exec( args, function( err, stdout, stderr) {
            if (err || stderr) {
                return callback( err || stderr.toString() );
            }
            else {
                //console.log(stdout.toString());
                var lines = stdout.toString().split(/\n/g);
                var resultList = [];
                for(var x = 0; x < lines.length; x++) {
                    if(lines[x] !== '') {
                        var parts = lines[x].split(/:/g);
                        var core = parts[0];
//                        var matches = parts[1].match(/\+(\*)\C/gi);
//                        console.log(matches);
                        var secondParts = parts[1].split(/\°\C  +/g);
                        var thirdParts = secondParts[0].split(/\+/g);
                        var temp = (thirdParts[1]);
                        resultList.push({'core': core, 'temp': temp});
                    }
                }

                return callback(false, resultList);
            }
        });
};

exports.nthAdapters = function(callback) {
    var ret = [];
    ChildProcess.exec( 'ifconfig | cut -c1-8 | sort -u | tr -d " "', function( err, stdout, stderr) {
        if (err || stderr) {
            return callback( err || stderr.toString() );
        }
        else {
            var ad = stdout.toString().split(/\n/g);
            var promises = [];
            for(var x = 0; x < ad.length; x++) {
                if(ad[x] !== '' && !ad[x].includes(':')) {
                    promises.push(Q.fcall(function (adapter, y) {
                        var deferred = Q.defer();
      
                        ChildProcess.exec( '/sbin/ifconfig '+adapter + "| grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}' | tr -d \"\n\r\"", function( err, ins, ine) {
                            if (err || stderr) {
                                return callback( err || stderr.toString() );
                            }
                            else {
                                var m = {};
                                m[adapter] = {'sent': 0, 'rcv': 0, 'ip': ins};
                                ret.push(m);
                                deferred.resolve(adapter);
                            }
                        });
                        
                        return deferred.promise;
                    }, ad[x], x));
                }
            }
            
            a = Q.all(promises);
            var y = 0;
            a.then(function (data) {
            }).done(function explode() {
                return callback(false, ret);
            });
        }
    });
};

exports.cat2 = function(args, x, eth, callback) {
    ChildProcess.exec( args, function( err, stdout, stderr) {
        if (err || stderr) {
            return callback( err || stderr.toString() );
        }
        else {
            //console.log(stdout.toString());
            var lines = stdout.toString().split(/\n/g);;
            var bytes = 0;
            for(var x = 0; x < lines.length; x++) {

                if(lines[x] !== '') {
                    bytes = bytes + parseInt(lines[x]);
                }
            }
//            console.log(eth);
            //var receivedBytes = lines[1];

            return callback(false, x, eth, bytes);
        }
    });
};

function cleanArray(actual){
  var newArray = new Array();
  for(var i = 0; i<actual.length; i++){
      if (actual[i]){
        newArray.push(actual[i]);
    }
  }
  return newArray;
}

exports.cat3 = function(args, callback) {
    ChildProcess.exec( args, function( err, stdout, stderr) {
        if (err || stderr) {
            return callback( err || stderr.toString() );
        }
        else {
            var ret = {
                'mem': {'total': 0, used: 0 , 'free': 0},
                'buffers': {'used': 0, free: 0},
                'swap': {'total': 0, used: 0 , 'free': 0}
            };
            var lines = stdout.toString().split(/\n/g);
            var bytes = 0;
            for(var x = 0; x < lines.length; x++) {
                var line = lines[x];
                var compontents = line.split(/ /g);
                compontents = cleanArray(compontents);
                //mem
                if(compontents[0] === 'Mem:') {
                    ret['mem']['total'] = compontents[1];
                    ret['mem']['used'] = compontents[2];
                    ret['mem']['free'] = compontents[3];
                }
                if(compontents[0] === 'Swap:') {
                    ret['swap']['total'] = compontents[1];
                    ret['swap']['used'] = compontents[2];
                    ret['swap']['free'] = compontents[3];
                }
                if(compontents[1] === 'buffers/cache:') {
                    ret['buffers']['used'] = compontents[2];
                    ret['buffers']['free'] = compontents[3];
                }
            }
  
            return callback(false, ret);
        }
    });
};