var prettyjson = require('prettyjson');
var begin = new Date().getTime();
var count = 0;
var emptyLines = 0;
var bytes = 0;
var logger = null;
var Tail = require('tail-forever');
var fs = require('fs');

var exports = module.exports = {};

/*
 * @filename location of the filename
 * @return fileSizeInBytes
 */
function getFilesizeInBytes(filename) {
    var stats = fs.statSync(filename)
    var fileSizeInBytes = stats['size']
    return fileSizeInBytes
}

function parseLine(line, sourceName, socket, logFile) {
    bytes += line.length
    count++
    var data = {'line': line, 'sourceName': sourceName, 'logFile': logFile}
    socket.emit('logs.parse.line', JSON.stringify(data));
    
//    if(count%100) {
//        stats();
//    }
    //la.parseLine(line, sourceName, cbf || log)
}

function log(err, data) {
    if (!data) {
        emptyLines++
        return
    }
    console.log(JSON.stringify(data))
}

function stats() {
    var duration = new Date().getTime() - begin
    var throughput = count / (duration / 1000)
    var throughputBytes = (bytes / 1024 / 1024) / (duration / 1000)

    console.error(duration + ' ms ' + count + ' lines parsed.  ' + throughput.toFixed(0) + ' lines/s ' + throughputBytes.toFixed(3) + ' MB/s - empty lines: ' + emptyLines)
    console.error('Heap Used: ' + (process.memoryUsage().heapUsed / (1024 * 1024)) + ' MB')
    console.error('Heap Total: ' + (process.memoryUsage().heapTotal / (1024 * 1024)) + ' MB')
    console.error('Memory RSS: ' + (process.memoryUsage().rss / (1024 * 1024)) + ' MB')

//    process.exit()
}

exports.tailFile = function (file, socket, logFile) {
    try {

        var tail = new Tail(file, {start: getFilesizeInBytes(file)})

        tail.on('line', function (line) {
            parseLine(line, file, socket, logFile)
        })
        tail.on('error', function (error) {
            console.log('ERROR: ', error)
        })
        console.log('Watching file:' + file)

        return tail
    } catch (e) {
        console.log('No such file ' + file)
    }
}