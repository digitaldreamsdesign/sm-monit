var ChildProcess = require('child_process');
var os = require("os");
var procfs = require('procfs-stats');

var key;
var config;
try {

    config = require('./config_dev.json');
}
catch (e) {
    config = require('./config.json');
}
if (process.argv.indexOf("-key") != -1) { //does our flag exist?
    key = process.argv[process.argv.indexOf("-key") + 1]; //grab the next item
    //var socket = require('socket.io-client')('http://109.123.123.228:1337?api=' + key);
    var socket = require('socket.io-client')('http://' + config.sm.ip + ':' + config.sm.port + '?api=' + key);
} else {
    console.log('You need to add -key');
    process.exit(0);
}

var utils = require("./lib/utils.js");
var logs = require("./lib/logs.js");

var version = '0.0.32';
var versionInt = 32;
var Q = require('q');

var policies = {'all': [], 'cpu_load': [], 'ram_usage': [], 'process_running': [], 'adapter_upload': [], 'adapter_download': []};
var adapters = [];

var updating = false;
var logFiles = [];
var logFilesClone = [];

utils.nthAdapters(function (err, list) {
    adapters = list;
});

function cloneObject(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }
 
    var temp = obj.constructor(); // give temp the original obj's constructor
    for (var key in obj) {
        temp[key] = cloneObject(obj[key]);
    }
 
    return temp;
}

socket.on('connect', function () {
    // socket connected
    socket.on('sm.connect', function (msg) {
        console.log(msg);
    });

    socket.on('sm.err', function (data) {
        console.log(data);
    });

    socket.on('event', function (msg) {
        console.log(msg);
    });

    socket.on('disconnect', function () {
        console.log('Disconected')
        process.exit();
    });

    setInterval(function () {
        socket.emit('monitor.alive', version);
    }, 5000);

    socket.on('start.monitoring', function (msg) {
        console.log(msg);
        processes();
        diskUsage();
    });

//    socket.on('version.check', function (vers) {
//        if (version !== vers && !updating) {
//            //we update
//            console.log('updating');
//            updating = true;
//            console.log('update from version '+ version + ' to version ' + vers);
//            var ChildProcess = require('child_process');
//            ChildProcess.exec('npm update webdilio-sm -g', function(err, stdout, stderr){
//                if (err !== null || stderr) {
//                    updating = false;
//                    console.log('exec error: ' + err + ' '+ stderr.toString());
//                }else {
//                    console.log(stdout);
//                    updating = false;
//                    process.exit();
//                }
//            });
//        }
//    });

//    socket.on('logs.send', function (data) {
//        var newFiles = JSON.parse(data);
////        console.log(newFiles);
//        var oldFiles = JSON.parse(JSON.stringify(logFiles));
////        var oldFilesClone = logFilesClone;
//        
//        var startTail = newFiles.filter(function(current){
//            return oldFiles.filter(function(current_b){
//                return current_b.id == current.id
//            }).length == 0
//        });
//        
//        var stopTail = oldFiles.filter(function(current){
//            return newFiles.filter(function(current_a){
//                return current_a.id == current.id
//            }).length == 0
//        });
//        
//        for (var x = 0; x < stopTail.length; x++) {
//            console.log('------------------------------');
//            console.log(stopTail[x]);
//            //stopTail[x]['tail'].unwatch();
//            if(stopTail[x] !== 'undefined') {
//                for (var y = 0; y < oldFiles.length; y++) {
//                    if(stopTail[x]['id'] == oldFiles[y]['id']) {
//                        delete oldFiles[y];
//                    }
//                }
//            }
//        }
//        
//        for (var x = 0; x < startTail.length; x++) {
//            startTail[x]['tail'] = logs.tailFile(startTail[x]['file_name'], socket, startTail[x]);
//            oldFiles.push(startTail[x]);
////            oldFilesClone.push(startTail[x]);
////            console.log(startTail[x]['tail']);
////            startTail[x]['tail'].unwatch();
////            console.log(startTail[x]['tail']);
//        }
//        
//        logFiles = oldFiles;
////        logFilesClone = oldFilesClone;
//    });
    
    socket.on('policy.send', function (data) {
        var len = JSON.parse(data);

        if (len.length != policies.all.length) {
            var oldData = policies.all;
            policies = {'all': [], 'cpu_load': [], 'ram_usage': [], 'process_running': [], 'adapter_upload': [], 'adapter_download': []};
            policies['all'] = JSON.parse(data);
            for (var x = 0; x < policies['all'].length; x++) {
                oldData.forEach(function (item) {
                    if (policies['all'][x]['id'] == item['id']) {
                        policies['all'][x]['status'] = item['status'];
                    }
                });

                if (policies['all'][x]['type'] === 'cpu_load') {
                    policies['cpu_load'].push(policies['all'][x]);
                }
                if (policies['all'][x]['type'] === 'ram_usage') {
                    policies['ram_usage'].push(policies['all'][x]);
                }
                if (policies['all'][x]['type'] === 'process_running') {
                    policies['process_running'].push(policies['all'][x]);
                }
                if (policies['all'][x]['type'] === 'adapter_upload') {
                    policies['adapter_upload'].push(policies['all'][x]);
                }
                if (policies['all'][x]['type'] === 'adapter_download') {
                    policies['adapter_download'].push(policies['all'][x]);
                }
            }
        }
    });
    var lastCpu = {
        'user': 0,
        'nice': 0,
        'system': 0,
        'idle': 0,
        'iowait': 0,
        'irq': 0,
        'softirq': 0,
        'steal': 0,
        'guest': 0,
        'guest_nice': 0
    }
    var prevIdle = 0;
    var prevNonIdle = 0;
    var prevTotal = 0;
    
    var timerInt = setInterval(function () {
        var cpus = os.cpus();
        var loadAvg = os.loadavg();
        var percent = loadAvg[0].toFixed(2) * 100 / cpus.length;
        var totalmem = os.totalmem();
        var freemem = os.freemem();
        var usedmem = totalmem - freemem;
        var freememPercent = (usedmem / totalmem) * 100.0;
        //console.log(freememPercent);
        var data = {
            'hostname': os.hostname(),
            'loadavg': os.loadavg(),
            'uptime': os.uptime(),
            'freemem': os.freemem(),
            'totalmem': os.totalmem(),
            'freememPercent': freememPercent,
            'cpus': os.cpus(),
            'type': os.type(),
            'release': os.release(),
            'networkInterfaces': os.networkInterfaces(),
            'arch': os.arch(),
            'platform': os.platform(),
            'cpuPercent': percent,
        };
        
        utils.cat3('free', function (err, ret) {
            if (err) {
                console.log(err);
            } else {
                if(ret['mem']['total'] > 0) {
                   data['freememPercent'] = ret['buffers']['used'] / ret['mem']['total'] * 100;
                }
                data['freeswapPercent'] = 0;
                if(ret['swap']['total'] > 0) {
                   data['freeswapPercent'] = ret['swap']['used'] / ret['swap']['total'] * 100;
                }
                
                data['ram'] = ret;
                
                procfs.cpu(function(err, list) {
                    if(err) {
                        
                    }else if(typeof list['cpu'] !== 'undefined') {
                        var curCpu = list['cpu'];
                        var nonIdle = parseInt(curCpu['user']) + parseInt(curCpu['nice']) + parseInt(curCpu['system']) + parseInt(curCpu['irq']) + parseInt(curCpu['softirq']) + parseInt(curCpu['steal']);
                        var idle = parseInt(curCpu['idle']) + parseInt(curCpu['iowait']);
                        var total = nonIdle + idle;
                        var totald = total - prevTotal;
                        var idled = idle - prevIdle;

                        prevTotal = total;
                        prevIdle = idle;
                        
                        var CPU_Percentage = (totald - idled)/totald * 100;
                        data['cpuPercent'] = CPU_Percentage;
                        data['cpus_details'] = list;
                    }
//                    console.log(data);
                    socket.emit('monitor', JSON.stringify(data));

                    /*
                     * check cpu load
                     */
                    for (var x = 0; x < policies['cpu_load'].length; x++) {
                        if (data['cpuPercent'] >= policies['cpu_load'][x]['cpu_open_percent'] && (policies['cpu_load'][x]['status'] === 'closed' || policies['cpu_load'][x]['status'] === 'not_verified')) {
                            //open the policy and add the incident
                            policies['cpu_load'][x]['status'] = 'opened';
                            var cpuLoad = {'policy': policies['cpu_load'][x], 'data': data, 'status': 'open'};
                            socket.emit('policy.cpu_load', JSON.stringify(cpuLoad));
                        }
                        if (data['cpuPercent'] < policies['cpu_load'][x]['cpu_close_percent'] && (policies['cpu_load'][x]['status'] === 'opened' || policies['cpu_load'][x]['status'] === 'not_verified')) {
                            //close the policy and close the incident
                            policies['cpu_load'][x]['status'] = 'closed';
                            var cpuLoad = {'policy': policies['cpu_load'][x], 'data': data, 'status': 'close'};
                            socket.emit('policy.cpu_load', JSON.stringify(cpuLoad));
                        }
                    }

                    for (var x = 0; x < policies['ram_usage'].length; x++) {
                        if (data['freememPercent'] >= policies['ram_usage'][x]['ram_open_percent'] && (policies['ram_usage'][x]['status'] === 'closed' || policies['ram_usage'][x]['status'] === 'not_verified')) {
                            //open the policy and add the incident
                            policies['ram_usage'][x]['status'] = 'opened';
                            var cpuLoad = {'policy': policies['ram_usage'][x], 'data': data, 'status': 'open'};
                            socket.emit('policy.ram_usage', JSON.stringify(cpuLoad));
                        }
                        if (data['freememPercent'] < policies['ram_usage'][x]['ram_close_percent'] && (policies['ram_usage'][x]['status'] === 'opened' || policies['ram_usage'][x]['status'] === 'not_verified')) {
                            //close the policy and close the incident
                            policies['ram_usage'][x]['status'] = 'closed';
                            var cpuLoad = {'policy': policies['ram_usage'][x], 'data': data, 'status': 'close'};
                            socket.emit('policy.ram_usage', JSON.stringify(cpuLoad));
                        }
                    }
                    
                });
            }
        });
        

    }, 3000);

    processes();
    var procInt = setInterval(processes, 30000);

    function processes() {
        utils.ps('ps auxc', function (err, list) {
            if (err) {
                console.log(err);
            } else {
                socket.emit('monitor.processes', JSON.stringify(list));

                /*
                 * 
                 */
                for (var x = 0; x < policies['process_running'].length; x++) {
                    var process = policies['process_running'][x]['process_name'];
                    var user = policies['process_running'][x]['process_user'];
                    var trigger = 1;
                    list.forEach(function (item) {
                        if (item['COMMAND'].toLowerCase() === process.toLowerCase() && item['USER'].toLowerCase() === user.toLowerCase()) {
                            trigger = 0;
                        }
                    });
                    if (trigger == 1 && (policies['process_running'][x]['status'] === 'closed' || policies['process_running'][x]['status'] === 'not_verified')) {
                        policies['process_running'][x]['status'] = 'opened';
                        var processRunning = {
                            'policy': policies['process_running'][x],
                            'data': {
                                'process': process,
                                'user': user
                            },
                            'status': 'open'};

                        socket.emit('policy.process_running', JSON.stringify(processRunning));
                    }
                    if (trigger == 0 && (policies['process_running'][x]['status'] === 'opened' || policies['process_running'][x]['status'] === 'not_verified')) {
                        policies['process_running'][x]['status'] = 'closed';
                        var processRunning = {
                            'policy': policies['process_running'][x],
                            'data': {
                                'process': process,
                                'user': user
                            },
                            'status': 'close'};

                        socket.emit('policy.process_running', JSON.stringify(processRunning));
                    }
                    console.log(process + ' ' + user + ' ' + ' ' + trigger)
                }
            }
        });
    }

    /*
     * get network usage
     */
    //netUsage();
    //var netInt = setInterval(netUsage, 4000);
    //var rcvFirst = 0;
    //var sentFirst = 0;
    //function netUsage() {
    function clone(a) {
        return JSON.parse(JSON.stringify(a));
    }
    var lastAdapters = {};
    var haveLast = false;
    setInterval(function () {
        var promises = [];
        for (var x = 0; x < adapters.length; x++) {
            promises.push(Q.fcall(function (adapter, y) {
                var deferred = Q.defer();
                var adName = Object.keys(adapter);
                utils.cat('cat /sys/class/net/' + adName + '/statistics/tx_bytes', function (err, sent) {
                    if (err) {
                        console.log(err);
                    } else {
                        //var list = {'rcv': (rcv-rcvFirst), 'sent': (sent-sentFirst)}
                        //rcvFirst = rcv;
                        //var a = adapters[y][adName]['sent'];
                        //console.log(adapters[y][adName]['sent']);
                        adapters[y][adName]['sent'] = sent*8;
                        //console.log(adapters[y][adName]['sent']);
                        utils.cat('cat /sys/class/net/' + adName + '/statistics/rx_bytes', function (err, rcv) {
                            if (err) {
                                console.log(err);
                            } else {
                                //console.log(rcv);
                                adapters[y][adName]['rcv'] = rcv*8;
                                deferred.resolve(1);
                            }
                        });
                    }
                });
                return deferred.promise;
            }, adapters[x], x));
        }
        //console.log(promises);
        a = Q.all(promises);
        var y = 0;
        a.then(function (data) {
            //console.log(data);
        }).done(function explode() {
            if (!haveLast) {
                lastAdapters = clone(adapters);
                //console.log(1111);
            }
            console.log(adapters);
            //console.log(adapters);
            //console.log(lastAdapters);
            socket.emit('monitor.net', JSON.stringify(adapters));
            if (haveLast) {
                //lastAdapters
                var netUsage = {};
                for (var x = 0; x < adapters.length; x++) {
                    for (var card in adapters[x]) {
                        if (typeof netUsage[card] === 'undefined') {
                            netUsage[card] = {};
                        }

                        netUsage[card]['sent'] = adapters[x][card]['sent'] - lastAdapters[x][card]['sent'];
                        netUsage[card]['rcv'] = adapters[x][card]['rcv'] - lastAdapters[x][card]['rcv'];
                    }
                }
                
                lastAdapters = clone(adapters);
                for (var x = 0; x < policies['adapter_upload'].length; x++) {
                    var open = policies['adapter_upload'][x]['upload_open'] * 1024 * 1024;
                    var close = policies['adapter_upload'][x]['upload_close'] * 1024 * 1024;
                    var card = policies['adapter_upload'][x]['card_name'];
                    if (typeof netUsage[card] !== 'undefined') {
                        if (netUsage[card]['sent'] >= open && (policies['adapter_upload'][x]['status'] === 'closed' || policies['adapter_upload'][x]['status'] === 'not_verified')) {
                            //close the policy and close the incident
                            policies['adapter_upload'][x]['status'] = 'opened';
                            var adapterUpload = {'policy': policies['adapter_upload'][x], 'data': netUsage[card], 'status': 'open'};
                            socket.emit('policy.adapter_upload', JSON.stringify(adapterUpload));
                        }
                        if (netUsage[card]['sent'] < close && (policies['adapter_upload'][x]['status'] === 'opened' || policies['adapter_upload'][x]['status'] === 'not_verified')) {
                            //close the policy and close the incident
                            policies['adapter_upload'][x]['status'] = 'closed';
                            var adapterUpload = {'policy': policies['adapter_upload'][x], 'data': netUsage[card], 'status': 'close'};
                            socket.emit('policy.adapter_upload', JSON.stringify(adapterUpload));
                        }
                    }
                }
                for (var x = 0; x < policies['adapter_download'].length; x++) {
                    var open = policies['adapter_download'][x]['download_open'] * 1024 * 1024;
                    var close = policies['adapter_download'][x]['download_close'] * 1024 * 1024;
                    var card = policies['adapter_download'][x]['card_name'];
                    if (typeof netUsage[card] !== 'undefined') {
                        if (netUsage[card]['rcv'] >= open && (policies['adapter_download'][x]['status'] === 'closed' || policies['adapter_download'][x]['status'] === 'not_verified')) {
                            //close the policy and close the incident
                            policies['adapter_download'][x]['status'] = 'opened';
                            var adapterDownload = {'policy': policies['adapter_download'][x], 'data': netUsage[card], 'status': 'open'};
                            socket.emit('policy.adapter_download', JSON.stringify(adapterDownload));
                        }
                        if (netUsage[card]['rcv'] < close && (policies['adapter_download'][x]['status'] === 'opened' || policies['adapter_download'][x]['status'] === 'not_verified')) {
                            //close the policy and close the incident
                            policies['adapter_download'][x]['status'] = 'closed';
                            var adapterDownload = {'policy': policies['adapter_download'][x], 'data': netUsage[card], 'status': 'close'};
                            socket.emit('policy.adapter_download', JSON.stringify(adapterDownload));
                        }
                    }
                }
            }
            haveLast = true;
        });
    }, 1000);
    //}

    /*
     * get server disk
     */
    var netInt = setInterval(diskUsage, 40000);
    diskUsage();
    function diskUsage() {
        utils.df('df', function (err, list) {
            if (err) {
                console.log(err);
            } else {
                socket.emit('monitor.df', JSON.stringify(list));
            }
        });
    }

    /*
     * get cpu temp
     */
    var cpuTempInt = setInterval(cpuTemp, 1000);
    cpuTemp();
    function cpuTemp() {
        utils.cpuTemp('sensors | grep "Core [0-9]"', function (err, list) {
            if (err) {
                console.log('CPU temp error');
                console.log(err);
            } else {

                socket.emit('monitor.cpu_temp', JSON.stringify(list));
            }
        });
    }

    var timerInt = setInterval(function () {
        var cpus = os.cpus();
        var loadAvg = os.loadavg();
        var percent = loadAvg[0].toFixed(2) * 100 / cpus.length;
        var data = {
            'hostname': os.hostname(),
            'loadavg': os.loadavg(),
            'uptime': os.uptime(),
            'freemem': os.freemem(),
            'totalmem': os.totalmem(),
            'cpus': os.cpus(),
            'type': os.type(),
            'release': os.release(),
            'networkInterfaces': os.networkInterfaces(),
            'arch': os.arch(),
            'platform': os.platform(),
            'cpuPercent': percent
        };
        socket.emit('incidents', JSON.stringify(data));
    }, 10000);
});

